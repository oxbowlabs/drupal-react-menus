import React from "react";
import ReactDOM from "react-dom";

// Menus
import Header from './components/Header.js';

// Styles
import "./styles.scss";

ReactDOM.render(
  <Header />,
  document.getElementById('react-header')
);
