// // Sample JSON
// export const urls = {
//   alerts: '/libraries/drupal_react_menus/src/json_sample/alerts.json',
//   lostFoundMenu: '/libraries/drupal_react_menus/src/json_sample/lost-found.json',
//   mainMenu: '/libraries/drupal_react_menus/src/json_sample/main-menu.json',
//   secondMenu: '/libraries/drupal_react_menus/src/json_sample/secondary.json'
// };

// // Local environment
// export const urls = {
//   alerts: '/api/alerts',
//   lostFoundMenu: '/api/menu_items/lost-found-menu',
//   mainMenu: '/api/menu_items/lost-found-menu',
//   secondMenu: '/api/menu_items/lost-found-menu'
// };

// Live URLs
export const urls = {
  alerts: 'https://pets.hsppr.org/api/alerts',
  // alerts: 'https://translate-p7ljbyq-7icme5wkumhao.us-2.platformsh.site/api/alerts',
  lostFoundMenu: 'https://www.hsppr.org/wp-json/menus/v1/menus/top-bar-menu-left',
  mainMenu: 'https://www.hsppr.org/wp-json/menus/v1/menus/main-menu',
  secondMenu: 'https://www.hsppr.org/wp-json/menus/v1/menus/TOP-BAR-menu-right'
  // lostFoundMenu: 'https://www.hsppr.org/wp-json/menus/v1/menus/top-bar-menu-left',
  // mainMenu: 'https://www.hsppr.org/wp-json/menus/v1/menus/main-menu',
  // secondMenu: 'https://www.hsppr.org/wp-json/menus/v1/menus/TOP-BAR-menu-right'
};

export const basicAuth = 'Basic ' + btoa("demo:staging");
// export const basicAuth = null; // when site is live
