import React from 'react';
import MenuItem from '../components/MenuItem.js';
import { urls, basicAuth } from "../config";
import 'whatwg-fetch';

class LostFoundMenu extends React.Component {
  state = {
    menu: []
  }

  render() {
    return(
      <React.Fragment>
        { this.state.menu.items !== undefined ? (
          <ul className="menu">
            {this.state.menu.items.map(function(menuItem, i) {
              return(
                <MenuItem key={i} item={menuItem} />
              )
            })}
          </ul>
        ): (null)}
      </React.Fragment>
    );
  }

  componentDidMount() {
    let url = urls.lostFoundMenu;
    let headers = new Headers();
    if (basicAuth) {
      headers.set('Authorization', basicAuth);
    }

    window.fetch(url, {
      method:'GET',
      // mode: 'no-cors',
      headers: headers,
    })
    // fetch(url)
    .then(res => res.json())
    .then((data) => {
      this.setState({ menu: data })
    })
    .catch(console.log)
  }
}

export default LostFoundMenu;
