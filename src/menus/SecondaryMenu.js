import React from 'react';
import MenuItem from '../components/MenuItem.js';
import { urls, basicAuth } from "../config";
import 'whatwg-fetch';

class SecondaryMenu extends React.Component {
  state = {
    menu: []
  }

  render() {
    return(
      <ul className="menu">
        { this.state.menu.items !== undefined ? (
          <React.Fragment>
            {this.state.menu.items.map(function(menuItem, i) {
              return(
                <MenuItem key={i} item={menuItem} />
              )
            })}
          </React.Fragment>
        ): (null)}
      </ul>
    );
  }

  componentDidMount() {
    let url = urls.secondMenu;
    let headers = new Headers();
    if (basicAuth) {
      headers.set('Authorization', basicAuth);
    }

    window.fetch(url, {
      method:'GET',
      // mode: 'no-cors',
      headers: headers,
    })
    .then(res => res.json())
    .then((data) => {
      this.setState({ menu: data })
    })
    .catch(console.log)
  }
}

export default SecondaryMenu;
