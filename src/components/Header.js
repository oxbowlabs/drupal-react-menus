import React from 'react';
import LostFoundMenu from './../menus/LostFoundMenu.js';
import SecondaryMenu from './../menus/SecondaryMenu.js';
import MainMenu from './../menus/MainMenu.js';
import Alerts from './../menus/Alerts.js';
import Logo from './Logo.js';

class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  googleTranslateElementInit () {
    new window.google.translate.TranslateElement({
      pageLanguage: 'en',
      includedLanguages: 'es,en',
      autoDisplay: true,
      multilanguagePage: true,
      gaTrack: true,
      gaId: 'UA-20955608-1'
    }, 'google_translate_element');
  }

  // Translation Happens when Main Menu is rendered (see menus/MainMenu.js)
  translate() {
    // console.log("translate");
    var addScript = document.createElement('script');
    addScript.setAttribute('src', '//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit');
    document.body.appendChild(addScript);
    window.googleTranslateElementInit = this.googleTranslateElementInit;
  }

  componentDidMount () {
    // console.log("componentDidMount");
  }

  render() {
    return(
      <React.Fragment>
      <div id="react-headers-container">
        <header id="header-before" className="header" role="banner" aria-label="Header Before">
          <div className="region region--header-before">
            <div id="react-lostfound-menu">
              <LostFoundMenu />
            </div>

            <div id="google_translate_element"></div>

            <div id="react-secondary-menu">
              <SecondaryMenu />
            </div>
          </div>
        </header>

        <header id="header" className="header" role="banner" aria-label="Site header">
          <div className="region region--header">
            <div id="block-hsppr-branding">
              <a href="https://www.hsppr.org" title="HSPPR Home Page" rel="home">
                <Logo />
              </a>
            </div>

            <div id="react-main-menu">
              <MainMenu
                googleTranslateElementInit = {this.googleTranslateElementInit}
                translate = {this.translate}
              />
            </div>
          </div>
        </header>
      </div>

      <Alerts />
      </React.Fragment>
    )
  }
}

export default Header;
