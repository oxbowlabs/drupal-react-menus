import React from 'react';
import MenuItem from './MenuItem.js';

class Menu extends React.Component {
  constructor(props) {
    super(props);
    this.state = props.menu;
  }

  render(key) {
    const menu = this.state;

    if (menu.child_items) {
      return(
        <ul className="menu">
          {menu.child_items.map(function(menuItem, i) {
            return(
              <MenuItem key={i} item={menuItem} />
            )
          })}
        </ul>
      )
    } else {
      return(null)
    }
  }
}

export default Menu;
