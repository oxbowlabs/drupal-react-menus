import React from 'react';

class MenuLink extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.state = props.link;
  }

  handleClick(e) {
    const link = this.state;

    if (link.child_items) {
      e.preventDefault();
    }

    else {
      e.stopPropagation();
    }
  }

  render() {
    const link = this.state;
    let classes = (link.classes ? link.classes.join(' ') : '');

    return (
      <a
        href={link.url}
        title={link.title}
        className={classes}
        target={link.target}
        onClick={this.handleClick}
        dangerouslySetInnerHTML={{__html: link.title}}
      >
      </a>
    );
  }
}

export default MenuLink;
