import React from 'react';
import MenuLink from './MenuLink.js';
import Menu from './Menu.js';
import HoverIntent from 'react-hoverintent';

class MenuItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = props.item;
    this.state.displayMenu = false;
  }

  show = () => {
    this.setState({ displayMenu: true });
  }

  hide = () => {
    this.setState({ displayMenu: false });
  }

  // TODO: Disable click for 1sec after hover
  handleOver = () => {
    if(window.innerWidth > 1150) {
      this.show();
    }
  }

  handleOut = () => {
    if(window.innerWidth > 1150) {
      this.hide();
    }
  }

  handleClick = () => {
    if (this.state.displayMenu == false) {
      this.show();
    }

    else {
      this.hide();
    }
  }

  render() {
    const item = this.state;

    // Wordpress Classes
    let wp_classes = (item.classes ? item.classes.join(' ') : '');

    // Has Children Class (menu-item--expanded)
    let app_classes = (item.child_items ? ' menu-item--expanded ' : '');

    // Show Children Class (menu-item--show-children)
    let display_classes = (item.displayMenu ? ' menu-item--show-children ' : '');

    // All Classes
    let classes = app_classes.concat(wp_classes, display_classes);

    return(
      <HoverIntent
        onMouseOver={this.handleOver}
        onMouseOut={this.handleOut}
        sensitivity={1}
        interval={0}
        timeout={50}
      >
      <li
        className={classes}
        onClick={this.handleClick}
      >
        <MenuLink
          link={item}
        />
          <Menu menu={item} />
      </li>
      </HoverIntent>
    );
  }
}

// { item.displayMenu ? (
//   <Menu menu={item} />
// ): (null)}

export default MenuItem;
