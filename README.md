# README #

To include the HSPPR navigation on a website, follow these steps:

1. Include the index.js file from /dist/index.js on your site somehow.  This can be as simple as downloading the file and loading it via a theme or a template file or in the header of your HTML document.

2. Add '<div id="react-header"></div>' in the template file where you'd like to render the react header.

3. If all goes well, you're done!

To work on the project, use npm:

'npm install'

'npm run dev' to compile dev build

'npm run watch' to watch with dev script

'npm run prod' for production build
